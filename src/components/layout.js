import React from "react"
import PropTypes from "prop-types"
import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import { Carousel } from 'react-responsive-carousel';
import { FaAngellist,FaMoon } from "react-icons/fa";
import Navbar from 'react-bootstrap/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-vertical-timeline-component/style.min.css';
import "./layout.css";

const Layout = ({ children }) => {
  return (
    <>
    <div>
      <Navbar style={{ background: '#252926' }}>
        <Navbar.Brand style={{ color: 'white' }}>To: Sirpa</Navbar.Brand>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text style={{ color: 'white' }}>

          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <div>
        <Carousel showThumbs={false}>

          <div>
            <p className="legend">This is yet another lame attempt to cool you off before i piss you off with pictures of people you adore! I am just a guy trying to make things right. in the language we  both speak.</p>

            <img src="https://wallpapercave.com/wp/vVLm6iJ.png" alt=''/>

          </div>
          <div>
            <img src="https://wallpapercave.com/wp/wp1827064.jpg" alt='' /> />
          </div>
          
          <div>
            <img src="https://images.unsplash.com/photo-1523749754744-f47b9c9b8df7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2689&q=80" alt='' />
          </div>
          <div>
            <img src="https://images.unsplash.com/photo-1571999001321-f54197a02cc8?ixlib=rb-1.2.1&auto=format&fit=crop&w=3300&q=80" alt='' />
          </div>
        </Carousel>
      </div>
      <div style={{ background: '#d4d4d4' }}>
        <VerticalTimeline layout={'1-column'}>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            contentArrowStyle={{ borderRight: '7px solid  rgb(33, 150, 243)' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaAngellist />}
          >
            <h3 className="vertical-timeline-element-title">So, Why am i doing this?</h3>
            <p> Honestly! I have no Fucking clue! </p>
            <p>Deep down my heart I just think that you are a difficult person to let go of! Doesn't really mean I have feeling for you one way or the other! On the whole as a person you have an impact on me and it's been difficult from my side to shake it off for a long time!'</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaMoon />}

          >
            <h3 className="vertical-timeline-element-title">So, Why am I doing this even after all the drama that happened</h3>
            <p> If I were to be honest with you! I don't exactly know the reason why I still try! But let me tell you what I did to hate you!</p>
            <br></br>
            <ul>
              <li>Well I convinced myself that you are jerk! and Ass but you've always been</li>
              <br></br>
              <li>Also as you know tried pissing you off with the picture! to see if i feel good! Well it did not feel good. Shocker!</li>
            </ul>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaAngellist />}

          >
            <h3 className="vertical-timeline-element-title">Why Should you Consider giving me a chance?</h3>
            
            <p> Because you are a good person 🧘, no seriously you are a good person! so just give me a chance and I won't mess up this time I promise :) </p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaMoon />}

          >
            <h3 className="vertical-timeline-element-title">Am I like all the creeps you've seen?</h3>
            <p> Well, At least I hope not. After all those things you told about them 😒 but if you think that, maybe i deserve to know that from you rather than nothing at all 😊</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaAngellist />}
          >
            <h3 className="vertical-timeline-element-title">How much do you mean to me?</h3>
            <p>Well! you are the only person I ever wanted to actually start a conversation with! not that you ever care!</p>
            <p>so You mean a Fair deal to me dude</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaMoon />}

          >
            <h3 className="vertical-timeline-element-title">Does that really matter, that you mean so much to me!</h3>
            <h4 className="vertical-timeline-element-subtitle">Certification</h4>
            <p>Actually No, All I need is a closure! that doesn't necessarily mean we just hug it out and forget what happened in the past year</p>
            <p>I am happy if you were to slap me and tell me how you feel</p>
          </VerticalTimelineElement>
          <VerticalTimelineElement
            className="vertical-timeline-element--education"
            contentStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
            icon={<FaAngellist />}

          >
            <h3 className="vertical-timeline-element-title">Conclusion!</h3>
            <p>I do love you as a person</p>
            <p>I Fucking Miss you dude</p>
            <p>But more than those I Respect the hell out of you dude! </p>
            <p>I just want to have a closure dude! That is all i need </p>
          </VerticalTimelineElement>

        </VerticalTimeline>
      </div>
    </div>
    </>

  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
